//
//  ViewController.m
//  p2p
//
//  Created by Developer on 4/30/13.
//  Copyright (c) 2013 Developer. All rights reserved.
//
#import "ViewController.h"
#import <ifaddrs.h>
#import <arpa/inet.h>
#import <UIKit/UIKit.h>
#import <CFNetwork/CFNetwork.h>
#import <Foundation/Foundation.h>
#import <dispatch/dispatch.h>
#import "/Users/developer/Downloads/CocoaAsyncSocket-master/GCD/GCDAsyncUdpSocket.h"
//#import "/Users/developer/Downloads/CocoaAsyncSocket-master/GCD/AsyncUdpSocket.h"
//#import "ThirdViewController.h"
//#import "DDLog.h"
//#import "DDTTYLogger.h"
//#import "GCDAsyncUdpSocket.h"
@interface ViewController ()

@end

@implementation ViewController
@synthesize ipServer;
@synthesize portServer;
@synthesize ipNat;
@synthesize portNat;
@synthesize Fname;
@synthesize webView;
@synthesize peer;
@synthesize message;
@synthesize inputStream;
@synthesize outputStream;

- (void)initNetworkCommunication {
    CFReadStreamRef readStream;
    CFWriteStreamRef writeStream;
    CFStreamCreatePairWithSocketToHost(NULL, (CFStringRef)@"31.170.160.209", 80, &readStream, &writeStream);
    inputStream=(__bridge_transfer NSInputStream *) readStream;
    outputStream = (__bridge_transfer NSOutputStream *)writeStream;
    [inputStream setDelegate:self];
    [outputStream setDelegate:self];
    [inputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    [outputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    [inputStream open];
    [outputStream open];
}


- (void)udpSocket:(GCDAsyncUdpSocket *)sock didReceiveData:(NSData *)data fromAddress:(NSData *)address withFilterContext:(id)filterContext
{
    NSString *msg;// = [[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] autorelease];
    
    NSString *host = nil;
    uint16_t port = 0;
    [GCDAsyncUdpSocket getHost:&host port:&port fromAddress:address];
    
    if (msg)
    {
        NSLog(@"Message = %@, Adress = %@ %i",msg,host,port);
    }
    else
    {
        NSLog(@"Error converting received data into UTF-8 String");
    }
}

- (void)stream:(NSStream *)stream handleEvent:(NSStreamEvent)eventCode {
    NSLog(@"stream:handleEvent: is invoked...");
    
    /*switch(eventCode) {
        case NSStreamEventHasSpaceAvailable:
        {
            if (stream == oStream) {
                NSString * str = [NSString stringWithFormat:
                                  @"GET / HTTP/1.0\r\n\r\n"];
                const uint8_t * rawstring =
                (const uint8_t *)[str UTF8String];
                [oStream write:rawstring maxLength:strlen(rawstring)];
                [oStream close];
            }
            break;
        }
            // continued ...
    }*/
   
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initNetworkCommunication];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(IBAction) SendToPeer:(id)sender{
    
    NSString *SMS=message.text;
    SMS = [SMS stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *peerAdd=Fname.text;
    peerAdd = [peerAdd stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    SMS = [SMS stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
   /* NSString *urlString = [NSString stringWithFormat:@"%@?+message=+%@+", peerAdd, SMS];
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    
    NSURLConnection *conn = [NSURLConnection connectionWithRequest:request delegate:self];
    
    if (conn == nil) {
        NSLog(@"cannot create connection");
    } */
    
    GCDAsyncUdpSocket *udpSocket ; // create this first part as a global variable
    udpSocket = [[GCDAsyncUdpSocket alloc] initWithDelegate:self delegateQueue:dispatch_get_main_queue()];
    
    NSData *data = [
                    [NSString stringWithFormat:SMS] dataUsingEncoding:NSUTF8StringEncoding];
    
    
    //[udpSocket sendData:data toHost:@"192.168.10.111" port:550 withTimeout:-1 tag:1];
    uint16_t PORT = [portNat.text intValue];
    
    //[udpSocket sendData:data toHost:@"192.168.10.111" port:550 withTimeout:-1 tag:1];
    [udpSocket sendData:data toHost:ipNat.text port:PORT withTimeout:-1 tag:1];
    
    
}
bool SW=FALSE;
-(IBAction) connRequest:(id)sender{
    NSString *pname=peer.text;
    NSString *fname=Fname.text;
    SW=TRUE;
    fname = [fname stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    pname = [pname stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *urlString = [NSString stringWithFormat:@"http://sohrab.comxa.com/saveip.php?id=%@&peer=%@", (fname),(pname)];
    //NSString *urlString = [NSString stringWithFormat:@"http://localhost:8888/conn_request.php?id=%@", (fname)];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    
	NSURLConnection *conn = [NSURLConnection connectionWithRequest:request delegate:self];
    if (conn == nil) {
        NSLog(@"cannot create connection");
    }
//initNetworkCommunication();
}
-(IBAction) SendToServer: (id)sender{
    
    NSString *fname=Fname.text;
    NSString *pname=Fname.text;
    fname = [fname stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    pname = [pname stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *urlString = [NSString stringWithFormat:@"http://sohrab.comxa.com/saveip.php?id=%@&peer=%@", (fname),(pname)];
    //NSString *urlString = [NSString stringWithFormat:@"http://localhost:8888/ipsave2.php?id=%@", (fname)];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    
	NSURLConnection *conn = [NSURLConnection connectionWithRequest:request delegate:self];
    if (conn == nil) {
        NSLog(@"cannot create connection");
    }
   
    /*GCDAsyncUdpSocket *udpSocket ;
    
    udpSocket = [[GCDAsyncUdpSocket alloc] initWithDelegate:self delegateQueue:dispatch_get_main_queue()];
    
    NSData *data =
    [
     [NSString stringWithFormat:@"id=ARAD&peer=ARAD"]
              dataUsingEncoding:NSUTF8StringEncoding
      ];

    
    //[udpSocket sendData:data toHost:@"192.168.10.111" port:550 withTimeout:-1 tag:1];
    uint16_t PORT = 80;
    [udpSocket sendData:data toHost:@"31.170.160.209" port:PORT withTimeout:-1 tag:1];
    
    
    
    //NSError * e;
    //NSData	     *data = [NSURLConnection sendAsynchronousRequest:request returningResponse:nil error:&e];
    
    //[self.webview loadRequest:request];
    //return [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
	/* NSHTTPURLResponse *response = nil;
     NSError *error = [[[NSError alloc] init] autorelease];
     [NSURLConnection sendAsynchronousRequest:request returningResponse:&response error:&error];*/

}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    //Convert to string and append received data
   // NSUInteger  an_Integer;
    NSArray * ipItemsArray;
    NSString *externalIP;
    
    
    NSString *theIpHtml = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
	[webView loadHTMLString:theIpHtml baseURL:nil];
                        NSScanner *theScanner;
            NSString *msg = [NSString stringWithFormat:@"message="];
            
            theScanner = [NSScanner scannerWithString:theIpHtml];
            
                ipItemsArray =[theIpHtml  componentsSeparatedByString:@"+"];
                externalIP =[ipItemsArray objectAtIndex:  1];
                      
            NSLog(@"%@",externalIP);
            Fname.text=[ipItemsArray objectAtIndex: 1 ];
            if(Fname.text==msg)
            {
                message.text=[ipItemsArray objectAtIndex: 2 ];
                return;
            }
            ipServer.text=[ipItemsArray objectAtIndex: 2 ];
            portServer.text=[ipItemsArray objectAtIndex: 3 ];
            ipNat.text=[ipItemsArray objectAtIndex: 4 ];
            portNat.text=[ipItemsArray objectAtIndex: 5 ];
    if(SW)
    {
        /*NSString *peerAdd=Fname.text;
        peerAdd = [peerAdd stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:peerAdd]];
        
        NSURLConnection *conn = [NSURLConnection connectionWithRequest:request delegate:self];*/
        
        GCDAsyncUdpSocket *udpSocket ; // create this first part as a global variable
        udpSocket = [[GCDAsyncUdpSocket alloc] initWithDelegate:self delegateQueue:dispatch_get_main_queue()];
        
        NSData *data = [
        [NSString stringWithFormat:@"+Hello World+"] dataUsingEncoding:NSUTF8StringEncoding];
        
        
        //[udpSocket sendData:data toHost:@"192.168.10.111" port:550 withTimeout:-1 tag:1];
        uint16_t PORT = [portNat.text intValue];
        
        //[udpSocket sendData:data toHost:@"192.168.10.111" port:550 withTimeout:-1 tag:1];
        [udpSocket sendData:data toHost:ipNat.text port:PORT withTimeout:-1 tag:1];
        SW=FALSE;
    }
       	
}
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    NSString *errorText;
    //Specific error
    if (error)
        errorText = [error localizedDescription];
    //Generic error
    else
        errorText = @"An error occurred . Please check that you are connected to the Internet";
    
    //Show error
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:errorText delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
   // [alert release];
    
    //Hide activity indicator
    //[self clearIssuesAccessoryView];
}


- (IBAction) getIPAddress:(id)sender
{ /*
        
    struct ifaddrs *interfaces = NULL;
    struct ifaddrs *temp_addr = NULL;
    NSString *wifiAddressUN = nil;
    NSString *cellAddressUN = nil;
    NSString *wifiAddress = nil;
    NSString *cellAddress = nil;
    
    // retrieve the current interfaces - returns 0 on success
    if(!getifaddrs(&interfaces)) {
        // Loop through linked list of interfaces
        temp_addr = interfaces;
        while(temp_addr != NULL) {
            sa_family_t sa_type = temp_addr->ifa_addr->sa_family;
            if(sa_type == AF_INET || sa_type == AF_INET6) {
                NSString *name = [NSString stringWithUTF8String:temp_addr->ifa_name];
                NSString *addr = [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_addr)->sin_addr)]; // pdp_ip0
                NSLog(@"NAME: \"%@\" addr: %@", name, addr); // see for yourself
                
                if([name isEqualToString:@"en0"] || [name isEqualToString:@"en1"]) {
                    // Interface is the wifi connection on the iPhone
                    wifiAddress = addr;
                    wifiAddressUN = [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_netmask)->sin_addr)];
                } else
                    if([name isEqualToString:@"pdp_ip0"]) {
                        // Interface is the cell connection on the iPhone
                        cellAddress = addr;
                        cellAddressUN = [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_netmask)->sin_addr)];                    }
            }
            temp_addr = temp_addr->ifa_next;
        }
        // Free memory
        freeifaddrs(interfaces);
    }
    //NSString *addr = wifiAddress ? wifiAddress : cellAddress;
    //addr ? addr : @"0.0.0.0";
    ip.text=cellAddress;
    ipw.text=wifiAddress;
    //ipUN.text=cellAddressUN;
    //ipwUN.text=wifiAddressUN;
    //ip.text=[NSString stringWithFormat:@"%f", 18.34];
        
    
    /////////////////////////////////////////////////////////////////////////
    
    NSUInteger  an_Integer;
    NSArray * ipItemsArray;
    NSString *externalIP;
    
    NSURL *iPURL = [NSURL URLWithString:@"http://www.dyndns.org/cgi-bin/check_ip.cgi"];
    
    if (iPURL) {
        NSError *error = nil;
        NSString *theIpHtml = [NSString stringWithContentsOfURL:iPURL
                                                       encoding:NSUTF8StringEncoding
                                                          error:&error];
        if (!error) {
            NSScanner *theScanner;
            NSString *text = nil;
            
            theScanner = [NSScanner scannerWithString:theIpHtml];
            
            while ([theScanner isAtEnd] == NO) {
                
                // find start of tag
                [theScanner scanUpToString:@"<" intoString:NULL] ;
                
                // find end of tag
                [theScanner scanUpToString:@">" intoString:&text] ;
                
                // replace the found tag with a space
                //(you can filter multi-spaces out later if you wish)
                theIpHtml = [theIpHtml stringByReplacingOccurrencesOfString:
                             [ NSString stringWithFormat:@"%@>", text]
                                                                 withString:@" "] ;
                ipItemsArray =[theIpHtml  componentsSeparatedByString:@" "];
                an_Integer=[ipItemsArray indexOfObject:@"Address:"];
                
                externalIP =[ipItemsArray objectAtIndex:  ++an_Integer];
                
                
                
            } 
            
            
            NSLog(@"%@",externalIP);
            publicIp.text=externalIP;
            
        } else {
            NSLog(@"Oops... g %d, %@", 
                  [error code], 
                  [error localizedDescription]);
        }
    }
    
    
    
    
    //[pool drain];  */
   
}
@end
