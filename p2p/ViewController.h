//
//  ViewController.h
//  p2p
//
//  Created by Developer on 4/30/13.
//  Copyright (c) 2013 Developer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CFNetwork/CFNetwork.h>
#import <Foundation/Foundation.h>
#import <dispatch/dispatch.h>
@interface ViewController : UIViewController  <NSStreamDelegate>
@property (retain, nonatomic) IBOutlet UITextField *ipServer;
@property (retain, nonatomic) IBOutlet UITextField *portServer;
@property (retain, nonatomic) IBOutlet UITextField *ipNat;
@property (retain, nonatomic) IBOutlet UITextField *portNat;
@property (retain, nonatomic) IBOutlet UITextField *Fname;
@property (retain, nonatomic) IBOutlet UITextField *peer;
@property (retain, nonatomic) IBOutlet UITextField *message;
@property NSInputStream *inputStream;
@property  NSOutputStream *outputStream;
//@property(nonatomic, retain) UIWebView *webview;
@property (retain,nonatomic)  IBOutlet UIWebView *webView;
-(IBAction) getIPAddress: (id)sender;
-(IBAction) SendToServer: (id)sender;
-(IBAction) SendToPeer: (id)sender;
-(IBAction) connRequest: (id)sender;
@end
